


if (Meteor.isClient) {

function getFormValuesAsJSON(values) { //Turns form data into structured data


  //Declare our new obects and arrays to return
  formReturn = {};
  formReturn.user = {};
  formReturn.formSubmission = {};
  formReturn.formSubmission.inputs = [];
  formReturn.formSubmission.fragments = [];

  //Do for loop through form input html objects
  var len = values.inputValues.length;
  for (i=0; i < len; i++) {
        formReturn.formSubmission.inputs[i] = new Object();
        formReturn.formSubmission.inputs[i].name = values.inputValues[i].name;
        formReturn.formSubmission.inputs[i].val = values.inputValues[i].value;
  }

  //Do for loop through form input html objects
  var leng = values.textareaValues.length;
  for (h=0; h < leng; h++) {
      formReturn.formSubmission.fragments[h] = new Object();
      formReturn.formSubmission.fragments[h].name = values.textareaValues[h].name;
      formReturn.formSubmission.fragments[h].val = values.textareaValues[h].value;
  }

  console.log(formReturn);

  return formReturn;

}

Template.editor.events({
    'click a#add-fragment': function () { //Add extra fragment to editor

    },
    'submit form': function (e) {
      event.preventDefault();
      values = {};
      console.log("lol the submission object");
      inputValues = e.target.querySelectorAll('input');
      textareaValues = e.target.querySelectorAll('textarea');

      values.inputValues = inputValues;
      values.textareaValues = textareaValues;
      console.log(values);
      //getFormValuesAsJSON(values);

      var structuredData = getFormValuesAsJSON(values);


      return false;

      //var JSONsubmission = getFormValuesAsJSON(e);
      //meteor.call('submit', JSONSubmission)
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
  Meteor.methods({

    'submitFormData': function() {

    }

  });
  /* I don't remember function syntax, but here's the idea
  function formSubmit() {

  }
  */
}
